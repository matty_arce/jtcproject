package com.jtcproject.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class AppSecurity extends WebSecurityConfigurerAdapter  {

	@Autowired
	private AuthenticationService authService;
	
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authService);
		//.and().withUser("dzarate").password("7890").roles("USER") ;
		//auth.jdbcAuthentication().dataSource(null).usersByUsernameQuery("select username, password, enabled from user where username =?")
		//.authoritiesByUsernameQuery("select username, role from username, role from user_roles where username =?");
		//auth.inMemoryAuthentication().withUser("matias").password("1234").roles("USER");
		//auth.jdbcAuthentication().dataSource(null).usersByUsernameQuery("select nrodocumento, pin from usuarios where nrodocumento = ?");
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/").permitAll()
				.anyRequest().authenticated()
			.and()
			.formLogin()
				.loginPage("/login").permitAll()
				.defaultSuccessUrl("/movimientos")
				//.usernameParameter("username")
				//.passwordParameter("password")
			.and()
			.logout().permitAll()
			.and()
			.csrf().disable();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring().antMatchers("/css/**", "/js/**");
	}

	
	
}
