package com.jtcproject.component;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ExternalBeans {

	@Bean
	public RestTemplate registrarRestTemplate(){
		return new RestTemplate();
	}
}
