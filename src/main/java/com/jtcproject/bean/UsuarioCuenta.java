package com.jtcproject.bean;

public class UsuarioCuenta {
	
	private Integer idFirma;
	private Integer idUsuario;
	private Integer idCuenta;

	public Integer getIdFirma() {
		return idFirma;
	}

	public void setIdFirma(Integer idFirma) {
		this.idFirma = idFirma;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Integer idCuenta) {
		this.idCuenta = idCuenta;
	}

}
