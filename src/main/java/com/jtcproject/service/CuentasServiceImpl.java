package com.jtcproject.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtcproject.bean.Cuentas;

@Service
public class CuentasServiceImpl implements CuentasService{

	private static final String REST_CTA_URL = "http://localhost:8090/rest/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override	
	public List<Cuentas> obtenerCuentas(){
		UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(REST_CTA_URL);
		urlBuilder.path("cuentas");
		Cuentas[] cuentasVector = restTemplate.getForObject(urlBuilder.toUriString(), Cuentas[].class);
		List<Cuentas> cuentasArrayList = Arrays.asList(cuentasVector);
		return cuentasArrayList;
	}
	
	@Override
	public Cuentas obtenerCuenta(Integer idCuenta){
		UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(REST_CTA_URL);
		Cuentas cuenta = restTemplate.getForObject(urlBuilder.path("/cuentas").path(String.valueOf(idCuenta)).toUriString(),
				Cuentas.class);
		return cuenta;
	}
}
