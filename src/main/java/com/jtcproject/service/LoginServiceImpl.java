package com.jtcproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

//import com.jtcproject.bean.User;
import com.jtcproject.bean.Usuarios;

@Service
public class LoginServiceImpl implements LoginService{
	
	private static final String REST_CONTACTO_URL = "http://localhost:8090/rest/login";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public boolean login(String user, String password){
		Usuarios usuario = new Usuarios();
		usuario.setNroDocumento(user);
		usuario.setPin(password);
		
		//usuario.setUsername(username);
		//usuario.setPassword(password);
		
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_CONTACTO_URL);
		//urlBuilder.path("username");
		
Boolean response = restTemplate.postForObject(urlBuilder.toUriString(), usuario, Boolean.class);
		
		return response;
	}
	
}
