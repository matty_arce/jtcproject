package com.jtcproject.service;

public interface LoginService {
	
	boolean login(String user, String password);

}
