package com.jtcproject.service;

import java.util.List;

import com.jtcproject.bean.Movimientos;

public interface MovimientosService {
	
	List<Movimientos> obtenerMovimientos();
	Movimientos obtenerMovimiento(Integer idCuenta); 

}
