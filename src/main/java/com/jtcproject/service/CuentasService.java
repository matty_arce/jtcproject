package com.jtcproject.service;

import java.util.List;

import com.jtcproject.bean.Cuentas;

public interface CuentasService {
	
	List<Cuentas> obtenerCuentas();
	Cuentas obtenerCuenta(Integer idCuenta);

}
