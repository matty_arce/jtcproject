package com.jtcproject.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jtcproject.bean.Movimientos;

@Service
public class MovimientosServiceImpl implements MovimientosService{

	private static final String REST_MOV_URL = "http://localhost:8090/rest/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public List<Movimientos> obtenerMovimientos(){
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_MOV_URL);
		urlBuilder.path("movimientos");
		Movimientos[] movimientosVector = restTemplate.getForObject(urlBuilder.toUriString(), Movimientos[].class );
		List<Movimientos> movimientosArrayList = Arrays.asList(movimientosVector);
		return movimientosArrayList;
	}
	
	@Override
	public Movimientos obtenerMovimiento(Integer idCuenta){
		UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(REST_MOV_URL);
		Movimientos movimiento = restTemplate.getForObject(urlBuilder.path("/movimientos")
				.path(String.valueOf(idCuenta)).toUriString(), Movimientos.class);
		
		return movimiento;
		
	}
	
}
