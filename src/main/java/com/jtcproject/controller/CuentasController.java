package com.jtcproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jtcproject.bean.Cuentas;
import com.jtcproject.service.CuentasService;

@Controller
@RequestMapping("/rest-client")
public class CuentasController {
	
	@Autowired
	private CuentasService cuentasService;
	
	@GetMapping
	public String index() {
		return "index";
	}
	
	@GetMapping("/cuentas")
	public String obtenerCuentas(Model model){
		List<Cuentas> listaCuentas = cuentasService.obtenerCuentas();
		model.addAttribute("cuentas", listaCuentas);
		return "lista";
	}
	
	

}
