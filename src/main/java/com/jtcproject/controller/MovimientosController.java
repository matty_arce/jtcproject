package com.jtcproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jtcproject.bean.Movimientos;
import com.jtcproject.service.MovimientosService;

@Controller
@RequestMapping("/movimientos")
public class MovimientosController {
	
	@Autowired
	private MovimientosService movimientosService;
	
	@GetMapping
	public String obtenerMovimientos(Model model){
		List<Movimientos> listaMovimientos = movimientosService.obtenerMovimientos();
		model.addAttribute("movimientos", listaMovimientos);
		
		return "listaMov";
	}

}
