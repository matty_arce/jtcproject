package com.jtcproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtcprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtcprojectApplication.class, args);
	}
}
